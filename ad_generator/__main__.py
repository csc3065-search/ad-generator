#!/usr/bin/python3
import os

from pymongo import MongoClient

from ad_generator.ad_gen import AdGen

MONGODB_URI = os.environ['MONGODB_URI']


def main():
    database = MongoClient(MONGODB_URI).get_default_database()
    vehicle_collection = database.get_collection('vehicles')
    ads_collection = database.get_collection('ads')
    ad_generator = AdGen(vehicle_collection, ads_collection)
    ad_generator.do_ad_generation()


if __name__ == '__main__':
    main()
