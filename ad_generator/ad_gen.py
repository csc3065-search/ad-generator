#!/usr/bin/python3
import io
import logging
import os
import random
import time
from typing import List

import boto3
import requests
import urllib3
from PIL import Image, ImageDraw, ImageFont
from pymongo.collection import Collection

logging.basicConfig(level=logging.INFO)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

FONT_BOLD = ImageFont.truetype('OpenSans-Bold.ttf', 50)
FONT_REGULAR = ImageFont.truetype('OpenSans-Regular.ttf', 40)
IMAGE_BUCKET_NAME = os.environ['IMAGE_BUCKET_NAME']
BASE_URL = os.environ['BASE_URL']


# noinspection PyMethodMayBeStatic
class AdGen:
    def __init__(self, vehicles_collection: Collection, ads_collection: Collection):
        self.vehicles_collection = vehicles_collection
        self.ads_collection = ads_collection

    def _find_applicable_vehicles(self) -> List[dict]:
        return self.vehicles_collection.aggregate([
            {'$match': {'$and': [{'image_urls.0': {'$exists': True}}, {'api_data.seller.name': {'$exists': True}}, {'api_data.vehicle.year': {'$exists': True}}]}},
            {'$sample': {'size': 80}}
        ])

    def _get_keywords_for_ad(self, vehicle: dict) -> List[str]:
        keywords = []
        api_data = vehicle['api_data']
        api_data_vehicle = api_data['vehicle']

        keywords.append(api_data_vehicle.get('make'))
        keywords.append(api_data_vehicle.get('model'))
        keywords.append(api_data_vehicle.get('trim'))

        return [k.strip().lower() for k in keywords if k is not None]

    def _create_ad_image(self, vehicle: dict, title: str):
        main_image_key = vehicle['image_urls'][0]
        r = requests.get(BASE_URL + '/img/vehicles/' + main_image_key.replace('{size}', 'medium'), verify=False, stream=True)
        r.raise_for_status()
        r.raw.decode_content = True
        main_img = Image.open(r.raw)
        canvas_img = Image.new(main_img.mode, (1480, 180))
        main_img = main_img.resize((256, 180))
        canvas_img.paste(main_img, box=(0, 0, 256, 180))
        draw = ImageDraw.Draw(canvas_img)
        draw.text((320, 10), title, font=FONT_BOLD, align='center')
        subtext = vehicle['api_data']['advert']['price'] + ' at ' + vehicle['api_data']['seller']['name']
        draw.text((320, 100), subtext, font=FONT_REGULAR)
        return canvas_img

    def _generate_ad_for(self, vehicle: dict):
        api_data = vehicle['api_data']
        api_data_vehicle = api_data['vehicle']
        keywords = self._get_keywords_for_ad(vehicle)
        make = api_data_vehicle['make']
        model = api_data_vehicle['model']
        year = api_data_vehicle['year']
        title = random.choice([
            f'Looking for a new {make} {model}?',
            f'Looking for a new {make}?',
            f'Interested in a new {make} {model}?',
            f'Thought of a new {make} {model}?',
            f'How about a new {make} {model}?',
            f'{make} {model}',
            f'The {make} {model}',
            f'{year} {make} {model}'
        ])
        return keywords, self._create_ad_image(vehicle, title)

    def _upload_ad(self, image: Image, ad_key: str):
        img_bytes = io.BytesIO()
        image.save(img_bytes, format='JPEG')
        s3 = boto3.client('s3')
        s3.put_object(Body=img_bytes.getvalue(), Key=ad_key, Bucket=IMAGE_BUCKET_NAME, ContentType='image/jpeg')

    def _delete_ad(self, key: str):
        s3 = boto3.client('s3')
        s3.delete_object(Key=key, Bucket=IMAGE_BUCKET_NAME)

    def do_ad_generation(self):
        applicable_vehicles = self._find_applicable_vehicles()
        existing_ad_ids = [a['_id'] for a in self.ads_collection.find({}, {'_id': 1})]

        for vehicle in applicable_vehicles:
            try:
                keywords, image = self._generate_ad_for(vehicle)
                ad_id = str(int(time.time() * 1000))
                ad_key = f'ads/{ad_id}.jpg'
                self._upload_ad(image, ad_key)
                self.ads_collection.insert_one({'_id': ad_id, 'keywords': keywords, 'image_url': ad_key, 'slug': vehicle['slug']})
                logging.info('Created an ad: ' + ad_key)
            except Exception as e:
                logging.error(e)
                continue

        self.ads_collection.delete_many({'_id': {'$in': existing_ad_ids}})

        # logging.info(f'Deleting {len(existing_ad_ids)} old ads..')
        # for ad_id in existing_ad_ids:
        #     self._delete_ad(f'ads/{ad_id}.jpg')