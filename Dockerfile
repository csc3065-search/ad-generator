FROM python:3.7-alpine as base
FROM base as builder

COPY requirements.txt /requirements.txt
RUN apk --update add python-dev jpeg-dev zlib-dev build-base g++ freetype-dev ttf-opensans && pip3 install -r /requirements.txt

COPY ad_generator /app/ad_generator

WORKDIR /app

CMD ["python3", "-m", "ad_generator"]
